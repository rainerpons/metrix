import { } from 'p5/global';

function setup(): void {
	createCanvas(windowHeight * 3 / 4, windowHeight * 3 / 4);
	background(100);
}

function draw(): void {
	ellipse(mouseX, mouseY, 20, 20);
}
